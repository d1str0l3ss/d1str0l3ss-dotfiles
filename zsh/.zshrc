HISTFILE=$XDG_CONFIG_HOME/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v

# The following lines were added by compinstall
zstyle ':completion:*' completer _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]} r:|[._-]=** r:|=**'
zstyle ':completion:*' max-errors 2
zstyle :compinstall filename '/home/th13rry/.config/zsh/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $XDG_CONFIG_HOME/zsh/plugins/zsh-autocomplete-terraform.zsh
source $XDG_CONFIG_HOME/zsh/themes/tokyonight.zsh


bindkey '^r'    history-incremental-search-backward
bindkey '^a'    beginning-of-line
bindkey '^[[1~' beginning-of-line
bindkey '^e'    end-of-line
bindkey '^[[4~' end-of-line
bindkey '^k'    kill-line
bindkey '^[[B'  history-search-forward
bindkey '^[[A'  history-search-backward
bindkey '^[[P'  delete-char

alias ls='exa --group-directories-first --icons'
alias ll='exa --group-directories-first -1 --icons'
alias man='batman'
alias cat='bat'
alias mime='xdg-mime query filetype'
alias e='nvim'
alias home='[ ! -z ${ENV_HOME} ] && cd ${ENV_HOME}'
alias phome='[ ! -z ${PROJECT_HOME} ] && cd ${PROJECT_HOME}'
alias parup='paru && pkill -RTMIN+17 dwmblocks'
alias xcl='xclip -selection c'
alias tf='terraform'
alias ck='git checkout'
alias ci='git commit'
alias pu='git push'
alias ga='git add'
alias gs='git status'
alias gp='git pull'
alias gl='git log --all --decorate --oneline --graph'
alias dlna='minidlnad -f $XDG_CONFIG_HOME/minidlna/minidlna.conf -P $XDG_CONFIG_HOME/minidlna/minidlna.pid -R'
alias ip='curl ipinfo.io/ip'
alias wratp='cd ~/work/ratp'
alias wlog='cd ~/work/log'
alias wpersonal='cd ~/work/personal'
alias rgb='sudo modprobe i2c-dev && modprobe i2c-piix4 && openrgb --noautoconnect --profile off'
# alias trnt='transmission-cli --no-downlimit --no-uplimit'

eval "$(starship init zsh)"
eval "$(direnv hook zsh)"
eval "$(atuin init zsh --disable-up-arrow)"
