# Dracula Theme (for zsh-syntax-highlighting)
#
# https://github.com/zenorocha/dracula-theme
#
# Copyright 2021, All rights reserved
#
# Code licensed under the MIT license
# http://zenorocha.mit-license.org
#
# @author George Pickering <@bigpick>
# @author Zeno Rocha <hi@zenorocha.com>
# Paste this files contents inside your ~/.zshrc before you activate zsh-syntax-highlighting
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main cursor)
typeset -gA ZSH_HIGHLIGHT_STYLES
# Default groupings per, https://spec.draculatheme.com, try to logically separate
# possible ZSH_HIGHLIGHT_STYLES settings accordingly...?
#
# Italics not yet supported by zsh; potentially soon:
#    https://github.com/zsh-users/zsh-syntax-highlighting/issues/432
#    https://www.zsh.org/mla/workers/2021/msg00678.html
# ... in hopes that they will, labelling accordingly with ,italic where appropriate
#
# Main highlighter styling: https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md
#
# Autosuggest
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#43434C'
## General
### Diffs
### Markup
## Classes
## Comments
ZSH_HIGHLIGHT_STYLES[comment]='fg=#43434C'

## Constants
## Entitites
## Functions/methods
ZSH_HIGHLIGHT_STYLES[alias]='fg=#C0A36E'
ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=#C0A36E'
ZSH_HIGHLIGHT_STYLES[global-alias]='fg=#C0A36E'
ZSH_HIGHLIGHT_STYLES[function]='fg=#C0A36E'
ZSH_HIGHLIGHT_STYLES[command]='fg=#C0A36E'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=#C0A36E,italic'
ZSH_HIGHLIGHT_STYLES[autodirectory]='fg=#727169,italic'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=#727169'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=#727169'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=#957FB9'
## Keywords
## Built ins
ZSH_HIGHLIGHT_STYLES[builtin]='fg=#C8C093'
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=#C8C093'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=#C8C093'
## Punctuation
ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=#7E9CD8'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]='fg=#7E9CD8'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-unquoted]='fg=#7E9CD8'
ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]='fg=#7E9CD8'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-delimiter]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=#98BB6C'
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]='fg=#98BB6C'
## Serializable / Configuration Languages
## Storage
## Strings
ZSH_HIGHLIGHT_STYLES[command-substitution-quoted]='fg=#E6C384'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-quoted]='fg=#E6C384'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=#98BB6C'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]='fg=#43434C'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=#98BB6C'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]='fg=#43434C'
ZSH_HIGHLIGHT_STYLES[rc-quote]='fg=#98BB6C'
## Variables
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument-unclosed]='fg=#43434C'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[assign]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[named-fd]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[numeric-fd]='fg=#DCD7BA'
## No category relevant in spec
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=#43434C'
ZSH_HIGHLIGHT_STYLES[path]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[path_pathseparator]='fg=#98BB6C'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[path_prefix_pathseparator]='fg=#98BB6C'
ZSH_HIGHLIGHT_STYLES[globbing]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=#957FB9'
#ZSH_HIGHLIGHT_STYLES[command-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[command-substitution-unquoted]='fg=?'
#ZSH_HIGHLIGHT_STYLES[process-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[arithmetic-expansion]='fg=?'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]='fg=#43434C'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[arg0]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[default]='fg=#DCD7BA'
ZSH_HIGHLIGHT_STYLES[cursor]='standout'

