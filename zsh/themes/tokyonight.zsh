# Dracula Theme (for zsh-syntax-highlighting)
#
# https://github.com/zenorocha/dracula-theme
#
# Copyright 2021, All rights reserved
#
# Code licensed under the MIT license
# http://zenorocha.mit-license.org
#
# @author George Pickering <@bigpick>
# @author Zeno Rocha <hi@zenorocha.com>
# Paste this files contents inside your ~/.zshrc before you activate zsh-syntax-highlighting
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main cursor)
typeset -gA ZSH_HIGHLIGHT_STYLES
# Default groupings per, https://spec.draculatheme.com, try to logically separate
# possible ZSH_HIGHLIGHT_STYLES settings accordingly...?
#
# Italics not yet supported by zsh; potentially soon:
#    https://github.com/zsh-users/zsh-syntax-highlighting/issues/432
#    https://www.zsh.org/mla/workers/2021/msg00678.html
# ... in hopes that they will, labelling accordingly with ,italic where appropriate
#
# Main highlighter styling: https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md
#
# Autosuggest
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#565f89'
## General
### Diffs
### Markup
## Classes
## Comments
ZSH_HIGHLIGHT_STYLES[comment]='fg=#565f89'
## Constants
## Entitites
## Functions/methods
ZSH_HIGHLIGHT_STYLES[alias]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[global-alias]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[function]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[command]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=#7dcfff,italic'
ZSH_HIGHLIGHT_STYLES[autodirectory]='fg=#e0af68,italic'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=#e0af68'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=#e0af68'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=#bb9af7'
## Keywords
## Built ins
ZSH_HIGHLIGHT_STYLES[builtin]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=#bb9af7'
## Punctuation
ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=#ff9e64'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]='fg=#ff9e64'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-unquoted]='fg=#ff9e64'
ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]='fg=#ff9e64'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-delimiter]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]='fg=#bb9af7'
## Serializable / Configuration Languages
## Storage
## Strings
ZSH_HIGHLIGHT_STYLES[command-substitution-quoted]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-quoted]='fg=#7dcfff'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]='fg=#f7768e'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]='fg=#f7768e'
ZSH_HIGHLIGHT_STYLES[rc-quote]='fg=#bb9af7'
## Variables
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument-unclosed]='fg=#f7768e'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[assign]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[named-fd]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[numeric-fd]='fg=#c0caf5'
## No category relevant in spec
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=#565f89'
ZSH_HIGHLIGHT_STYLES[path]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[path_pathseparator]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[path_prefix_pathseparator]='fg=#bb9af7'
ZSH_HIGHLIGHT_STYLES[globbing]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=#bb9af7'
#ZSH_HIGHLIGHT_STYLES[command-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[command-substitution-unquoted]='fg=?'
#ZSH_HIGHLIGHT_STYLES[process-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[arithmetic-expansion]='fg=?'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]='fg=#565f89'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[arg0]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[default]='fg=#c0caf5'
ZSH_HIGHLIGHT_STYLES[cursor]='standout'

