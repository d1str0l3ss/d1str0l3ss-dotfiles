export EDITOR="nvim"
export TERMINAL="st"

# XDG configs
export XDG_DATA_HOME=$(pwd)/.local/share
export XDG_CONFIG_HOME=$(pwd)/.config
export XDG_CACHE_HOME=$(pwd)/.cache
export XDG_STATE_HOME=$XDG_DATA_HOME/state
export XDG_USR_BIN=$(pwd)/.local/bin

# Nvidia config
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv/ComputeCache"
export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
export __GL_SHADER_DISK_CACHE_SIZE=100000000000

export PATH=$PATH:$XDG_USR_BIN

# EXA base
export EXA_COLORS=".*=1;30:di=1;37:da=1;37:ex=1;34"
# EXA size
export EXA_COLORS="${EXA_COLORS}:sn=1;34:sb=1;34:df=1;34:ds=1;34"
# EXA permissions
export EXA_COLORS="${EXA_COLORS}:ur=1;37:uw=1;35:ux=1;34:ue=1;34:gr=1;37:gw=1;35:gx=1;34:tr=1;37:tw=1;31:tx=1;31"
# EXA links
export EXA_COLORS="${EXA_COLORS}:ln=1;35:lp=1;35"
# EXA pictures 
export EXA_COLORS="${EXA_COLORS}:*.arw=;36:*.bmp=;36:*.cbr=;36:*.cbz=;36:*.cr2=;36:*.dvi=;36:*.eps=;36:*.gif=;36:*.heif=;36:*.ico=;36:*.jpeg=;36:*.jpg=;36:*.nef=;36:*.orf=;36:*.pbm=;36:*.pgm=;36:*.png=;36:*.pnm=;36:*.ppm=;36:*.ps=;36:*.raw=;36:*.stl=;36:*.svg=;36:*.tif=;36:*.tiff=;36:*.webp=;36:*.xpm=;36"
# EXA videos
export EXA_COLORS="${EXA_COLORS}:*.avi=;35:*.flv=;35:*.heic=;35:*.m2ts=;35:*.m2v=;35:*.mkv=;35:*.mov=;35:*.mp4=;35:*.mpeg=;35:*.mpg=;35:*.ogm=;35:*.ogv=;35:*.ts=;35:*.vob=;35:*.webm=;35:*.wmv=;35"
# EXA musics
export EXA_COLORS="${EXA_COLORS}:*.aac=;35:*.alac=;35:*.ape=;35:*.flac=;35:*.m4a=;35:*.mka=;35:*.mp3=;35:*.ogg=;35:*.opus=;35:*.wav=;35:*.wma=;35"
#EXA archives
export EXA_COLORS="${EXA_COLORS}:*.7z=36:*.a=36:*.ar=36:*.bz2=36:*.deb=36:*.dmg=36:*.gz=36:*.iso=36:*.lzma=36:*.par=36:*.rar=36:*.rpm=36:*.tar=36:*.tc=36:*.tgz=36:*.txz=36:*.xz=36:*.z=36:*.Z=36:*.zip=36:*.zst=36"

export BAT_THEME="tokyonight"

# HOME cleanup
export WGETRC=$XDG_CONFIG_HOME/wget/wgetrc
export ZDOTDIR=$XDG_CONFIG_HOME/zsh
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/config
export CARGO_HOME=$XDG_CACHE_HOME/cargo
