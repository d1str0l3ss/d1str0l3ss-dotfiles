local terraformGroup = vim.api.nvim_create_augroup('TerraformFiletypes', { clear = true })
vim.api.nvim_create_autocmd(
  {
    'BufRead' ,
    'BufNewFile'
  }, {
    pattern = {
      '*.hcl',
      '.terraformrc',
      'terraform.rc',
      --'*.tf',
      '*.tfvars'
    },
    command = 'set filetype=hcl',
    group = terraformGroup
  }
)
