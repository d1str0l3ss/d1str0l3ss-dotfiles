-- Status
vim.opt.showmode = false
vim.opt.laststatus = 3

-- Colors
vim.opt.termguicolors = true
vim.opt.cursorline = true

-- Indenting
vim.opt.expandtab = true
vim.opt.shiftwidth = 2
vim.opt.smartindent = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2

-- Numbers
vim.wo.relativenumber = true
vim.opt.number = true
vim.opt.numberwidth = 4
vim.opt.ruler = true

-- Split
vim.opt.splitbelow = true
vim.opt.splitright = true

-- History
vim.opt.undofile = true
vim.opt.updatetime = 250

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldenable = false

vim.opt.hlsearch = false
vim.opt.list = true
