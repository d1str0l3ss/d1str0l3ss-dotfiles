return {
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    event = {
      "BufReadPost",
      "BufNewFile"
    },
    config = function()
      require('nvim-treesitter.configs').setup({
        ensure_installed = {
          'comment',
          'c',
          'make',
          'markdown',
          'markdown_inline',
          'python',
          'rust',
          'bash',
          'dockerfile',
          'lua',
          'vim',
          'vimdoc',
          'query',
          'hcl',
          'terraform',
          'json',
          'groovy',
          'typescript',
        },
        highlight = {
          enable = true
        },
        indent = {
          enable = false
        },
      })
    end
  },
  {
    'towolf/vim-helm',
  }
}
