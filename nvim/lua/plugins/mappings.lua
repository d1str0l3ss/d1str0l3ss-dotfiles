-- return {}
-- This plugin seems to break some macros (ex: advent of code)
return {
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    -- keys = { '<leader>', '"', 'c', 'g', 'v' },
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 500
    end,
    config = function()
      require('which-key').setup()
    end
  }
}
