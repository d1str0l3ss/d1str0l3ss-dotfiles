return {
  {
    'jose-elias-alvarez/null-ls.nvim',
    event = 'VeryLazy',
    config = function ()
      local null_ls = require('null-ls')
      local augroup = vim.api.nvim_create_augroup('LspFormatting', {})

      null_ls.setup({
        sources = {
          null_ls.builtins.diagnostics.terraform_validate,
          null_ls.builtins.formatting.terraform_fmt
        },
        on_attach = function(client, bufnr)
          if client.supports_method('textDocument/formatting') then
            vim.api.nvim_clear_autocmds({
              group = augroup,
              buffer =  bufnr
            })
            vim.api.nvim_create_autocmd('BufWritePre', {
              group = augroup,
              buffer = bufnr,
              callback = function()
                vim.lsp.buf.format({bufnr = bufnr})
              end
            })
          end
        end
      })
    end
  },
  {
    'williamboman/mason.nvim',
    build = ':MasonUpdate',
    config = function()
      require('mason').setup()
    end
  },
  {
    'williamboman/mason-lspconfig.nvim',
    config = function()
      require('mason-lspconfig').setup({
        ensure_installed = {
          'lua_ls',
          'bashls',
          'clangd',
          'dockerls',
          'gradle_ls',
          'jsonls',
          'marksman',
          'rust_analyzer',
          'taplo',
          'terraformls',
          'tflint',
          'tsserver',
          'helm_ls'
        }
      })
    end
  },
  {
    'neovim/nvim-lspconfig',
    config = function()
      require('lspconfig').lua_ls.setup({
        settings = {
          Lua = {
            diagnostics = {
              globals = { 'vim' }
            }
          }
        }
      })
      require('lspconfig').clangd.setup({})
      require('lspconfig').bashls.setup({})
      require('lspconfig').dockerls.setup({})
      require('lspconfig').gradle_ls.setup({})
      require('lspconfig').jsonls.setup({})
      require('lspconfig').marksman.setup({})
      require('lspconfig').rust_analyzer.setup({})
      require('lspconfig').taplo.setup({})
      require('lspconfig').terraformls.setup({})
      require('lspconfig').tflint.setup({})
      require('lspconfig').tsserver.setup({})
      require('lspconfig').helm_ls.setup({})
    end
  },
}
