return {
  {
    'nvim-lualine/lualine.nvim',
    config = function()
      require('lualine').setup({
        options = { section_separators = '', component_separators = '', theme = 'tokyonight' },
        sections = { lualine_c = {'buffers'} },
        extensions = { 'lazy', 'quickfix' }
      })
    end
  }
}
