return {
  {
    'kylechui/nvim-surround',
    event = 'VeryLazy',
    config = function()
      require('nvim-surround').setup()
    end
  },
  {
    'numToStr/Comment.nvim',
    event = 'VeryLazy',
    config = function()
      require('Comment').setup()
    end
  },
  {
    'lukas-reineke/indent-blankline.nvim',
    event = {
      'BufReadPost',
      'BufNewFile'
    },
    config = function()
      require('indent_blankline').setup({
        vim.api.nvim_set_hl(0, 'IndentBlanklineContextChar', { link = 'LspInfoBorder' }),
        char = '┆',
        show_current_context = true,
      })
    end
  },
}
